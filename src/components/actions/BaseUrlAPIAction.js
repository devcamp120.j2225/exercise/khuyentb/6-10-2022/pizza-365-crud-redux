import {
    ORDER_BASE_URL,
    DRINK_BASE_URL,
    VOUCHER_BASE_URL,
    FETCH_DRINK_ERROR,
    FETCH_DRINK_PENDING,
    FETCH_DRINK_SUCCESS,
    FETCH_DRINK_SELECT,
    FETCH_ORDER_ERROR,
    FETCH_ORDER_PENDING,
    FETCH_ORDER_SUCCESS,
    FETCH_VOUCHER_ERROR,
    FETCH_VOUCHER_PENDING,
    FETCH_VOUCHER_SUCCESS
} from "../constants/BaseUrlAPIConstant"

const fetchOrderAPI = (body) => async dispatch => {

    

    await dispatch({
        type: FETCH_ORDER_PENDING
    })

    try {
        const response = await fetch(
            ORDER_BASE_URL, body
        );

        const data = await response.json();

        return dispatch({
            type: FETCH_ORDER_SUCCESS,
            data: data
        })
    } catch (err) {
        return dispatch({
            type: FETCH_ORDER_ERROR,
            error: err
        })
    }
}

const fetchVoucherAPI = (vVoucherId) => async dispatch => {

    var vVoucherOptions = {
        method: "GET",
        redirect: "follow"
    }

    await dispatch({
        type: FETCH_VOUCHER_PENDING
    })

    try {
        const response = await fetch(
            VOUCHER_BASE_URL + vVoucherId, vVoucherOptions
        );

        const data = await response.json();

        return dispatch({
            type: FETCH_VOUCHER_SUCCESS,
            data: data
        })
    } catch (err) {
        return dispatch({
            type: FETCH_VOUCHER_ERROR,
            error: err
        })
    }
}

const fetchDrinkAPI = () => async dispatch => {

    var vDrinkOptions = {
        method: "GET",
        redirect: "follow"
    }

    await dispatch({
        type: FETCH_DRINK_PENDING
    })

    try {
        const response = await fetch(
            DRINK_BASE_URL, vDrinkOptions
        );

        const data = await response.json();

        return dispatch({
            type: FETCH_DRINK_SUCCESS,
            data: data
        });
    } catch (err) {
        return dispatch({
            type: FETCH_DRINK_ERROR,
            error: err
        })
    }
}

const DrinkSelect = (value) => {
    return {
        type: FETCH_DRINK_SELECT,
        payload: value
    }
}

export {
    fetchDrinkAPI,
    fetchOrderAPI,
    fetchVoucherAPI,
    DrinkSelect
}
