import {
    CREATE_FORM_NAME,
    CREATE_FORM_EMAIL,
    CREATE_FORM_PHONE_NUMBER,
    CREATE_FORM_ADDRESS,
    CREATE_FORM_VOUCHER,
    CREATE_FORM_MESSAGE
} from "../constants/OrderFormConstant"

const nameInput = (value) => {
    return {
        type: CREATE_FORM_NAME,
        payload: value
    }
}

const emailInput = (value) => {
    return {
        type: CREATE_FORM_EMAIL,
        payload: value
    }
}

const phoneNumberInput = (value) => {
    return {
        type: CREATE_FORM_PHONE_NUMBER,
        payload: value
    }
}

const addressInput = (value) => {
    return {
        type: CREATE_FORM_ADDRESS,
        payload: value
    }
}

const voucherInput = (value) => {
    return {
        type: CREATE_FORM_VOUCHER,
        payload: value
    }
}

const messageInput = (value) => {
    return {
        type: CREATE_FORM_MESSAGE,
        payload: value
    }
}

export {
    nameInput,
    emailInput,
    phoneNumberInput,
    addressInput,
    voucherInput,
    messageInput
}