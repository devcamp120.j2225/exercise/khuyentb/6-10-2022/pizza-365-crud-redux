import {
    PIZZA_COMBO_LARGE,
    PIZZA_COMBO_MEDIUM,
    PIZZA_COMBO_SMALL
} from "../constants/PizzaComboSelectConstant"

function selectComboSmall () {
    return {
        type: PIZZA_COMBO_SMALL,
        payload: {
            kichCo: "Small",
            duongKinh: "20",
            suon: 2,
            salad: "200",
            soLuongNuoc: 2,
            thanhTien: 150000
        }
    }
}

function selectComboMedium () {
    return {
        type: PIZZA_COMBO_MEDIUM,
        payload: {
            kichCo: "Medium",
            duongKinh: "25",
            suon: 4,
            salad: "300",
            soLuongNuoc: 3,
            thanhTien: 200000
        }
    }
}

function selectComboLarge () {
    return {
        type: PIZZA_COMBO_LARGE,
        payload: {
            kichCo: "Large",
            duongKinh: "30",
            suon: 8,
            salad: "500",
            soLuongNuoc: 4,
            thanhTien: 250000
        }
    }
}

export {
    selectComboSmall,
    selectComboMedium,
    selectComboLarge
}