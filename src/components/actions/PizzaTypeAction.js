import {
    PIZZA_TYPE_SEAFOOD,
    PIZZA_TYPE_HAWAII,
    PIZZA_TYPE_BACON
} from "../constants/PizzaTypeConstant"

const selectTypeSeafood = () => {
    return {
        type: PIZZA_TYPE_SEAFOOD,
        payload: "Seafood"
    }
}

const selectTypeHawaii = () => {
    return {
        type: PIZZA_TYPE_HAWAII,
        payload: "Hawaii"
    }
}

const selectTypeBacon = () => {
    return {
        type: PIZZA_TYPE_BACON,
        payload: "Bacon"
    }
}

export {
    selectTypeBacon,
    selectTypeHawaii,
    selectTypeSeafood
}