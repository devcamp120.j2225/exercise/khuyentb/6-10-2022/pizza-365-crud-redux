import { combineReducers } from "redux";
import CreateOrderFormReducer from "./CreateOrderFormReducer";
import DrinkReducer from "./DrinkReducer";
import OrderFormReducer from "./OrderFormReducer";
import PizzaComboSelectReducer from "./PizzaComboSelectReducer";
import PizzaTypeReducer from "./PizzaTypeReducer";
import VoucherReducer from "./VoucherReducer";

const rootReducer = combineReducers(
    {DrinkReducer,
    VoucherReducer,
    OrderFormReducer,
    PizzaComboSelectReducer,
    PizzaTypeReducer,
    CreateOrderFormReducer}
)

export default rootReducer