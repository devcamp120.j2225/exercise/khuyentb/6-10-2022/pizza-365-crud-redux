import {
    FETCH_DRINK_PENDING,
    FETCH_DRINK_SUCCESS,
    FETCH_DRINK_ERROR,
    FETCH_DRINK_SELECT
} from "../constants/BaseUrlAPIConstant";

const initialState = {
    drinks: [],
    pending: false,
    error: null,
    drinkSelected: ""
}

function DrinkReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_DRINK_PENDING:
            return {
                ...state,
                pending: true
            }
        case FETCH_DRINK_SUCCESS:
            return {
                ...state,
                drinks: action.data
            }
        case FETCH_DRINK_ERROR:
            return {
                ...state,
                error: action.error
            }
        case FETCH_DRINK_SELECT:
            return {
                ...state,
                drinkSelected: action.payload
            }
        default:
            return state
    }
}

export default DrinkReducer;