import {
    FETCH_ORDER_ERROR,
    FETCH_ORDER_PENDING,
    FETCH_ORDER_SUCCESS,
} from "../constants/BaseUrlAPIConstant"

const initialState = {
    orders: [],
    pending: false,
    error: null
}

function OrderFormReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_ORDER_SUCCESS:
            return {
                ...state,
                orders: action.data
            }
        case FETCH_ORDER_PENDING:
            return {
                ...state,
                pending: true
            }
        case FETCH_ORDER_ERROR:
            return {
                ...state,
                error: action.error
            }
        default:
            return state
    }
}

export default OrderFormReducer