import {
    CREATE_FORM_NAME,
    CREATE_FORM_EMAIL,
    CREATE_FORM_PHONE_NUMBER,
    CREATE_FORM_ADDRESS,
    CREATE_FORM_VOUCHER,
    CREATE_FORM_MESSAGE
} from "../constants/OrderFormConstant"

const intialState = {
    fullName: "",
    email: "",
    phoneNumber: "",
    address: "",
    maGiamGia: "",
    message: ""
}

function CreateOrderFormReducer(state = intialState, action) {
    switch (action.type) {
        case CREATE_FORM_NAME:
            return {
                ...state,
                fullName: action.payload
            }
        case CREATE_FORM_EMAIL:
            return {
                ...state,
                email: action.payload
            }
        case CREATE_FORM_PHONE_NUMBER:
            return {
                ...state,
                phoneNumber: action.payload
            }
        case CREATE_FORM_ADDRESS:
            return {
                ...state,
                address: action.payload
            }
        case CREATE_FORM_VOUCHER:
            return {
                ...state,
                maGiamGia: action.payload
            }
        case CREATE_FORM_MESSAGE:
            return {
                ...state,
                message: action.payload
            }
        default:
            return state
    }
}

export default CreateOrderFormReducer