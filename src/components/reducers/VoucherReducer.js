import {
    FETCH_VOUCHER_ERROR,
    FETCH_VOUCHER_PENDING,
    FETCH_VOUCHER_SUCCESS
} from "../constants/BaseUrlAPIConstant"

const initialState = {
    voucherResult: [],
    pending: false,
    error: null
}

function VoucherReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_VOUCHER_SUCCESS:
            return {
                ...state,
                voucherResult: action.data
            }
        case FETCH_VOUCHER_PENDING:
            return {
                ...state,
                pending: true
            }
        case FETCH_VOUCHER_ERROR:
            return {
                ...state,
                error: action.error
            }
        default:
            return state
    }
}

export default VoucherReducer;