import {
    PIZZA_TYPE_SEAFOOD,
    PIZZA_TYPE_HAWAII,
    PIZZA_TYPE_BACON
} from "../constants/PizzaTypeConstant"

const initialState = {
    loaiPizza: ""
}

function PizzaTypeReducer(state = initialState, action) {
    switch (action.type) {
        case PIZZA_TYPE_SEAFOOD:
            return {
                ...state,
                loaiPizza: action.payload
            }
        case PIZZA_TYPE_HAWAII:
            return {
                ...state,
                loaiPizza: action.payload
            }
        case PIZZA_TYPE_BACON:
            return {
                ...state,
                loaiPizza: action.payload
            }
        default:
            return state
    }
}

export default PizzaTypeReducer