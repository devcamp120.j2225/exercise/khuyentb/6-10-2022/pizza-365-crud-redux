import {
    PIZZA_COMBO_LARGE,
    PIZZA_COMBO_MEDIUM,
    PIZZA_COMBO_SMALL
} from "../constants/PizzaComboSelectConstant";

const initialState = {
    kichCo: "",
    duongKinh: "",
    suon: "",
    salad: "",
    soLuongNuoc: "",
    thanhTien: ""
};

function PizzaComboSelectReducer(state = initialState, action) {
    switch (action.type) {
        case PIZZA_COMBO_SMALL:
            return {
                ...state,
                kichCo: action.payload.kichCo,
                duongKinh: action.payload.duongKinh,
                suon: action.payload.suon,
                salad: action.payload.salad,
                soLuongNuoc: action.payload.soLuongNuoc,
                thanhTien: action.payload.thanhTien
            };
        case PIZZA_COMBO_MEDIUM:
            return {
                ...state,
                kichCo: action.payload.kichCo,
                duongKinh: action.payload.duongKinh,
                suon: action.payload.suon,
                salad: action.payload.salad,
                soLuongNuoc: action.payload.soLuongNuoc,
                thanhTien: action.payload.thanhTien
            };
        case PIZZA_COMBO_LARGE:
            return {
                ...state,
                kichCo: action.payload.kichCo,
                duongKinh: action.payload.duongKinh,
                suon: action.payload.suon,
                salad: action.payload.salad,
                soLuongNuoc: action.payload.soLuongNuoc,
                thanhTien: action.payload.thanhTien
            };
        default:
            return state
    }
}

export default PizzaComboSelectReducer