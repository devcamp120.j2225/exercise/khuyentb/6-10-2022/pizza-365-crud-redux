import ContentComponent from "./Content/ContentComponent";
import FooterComponent from "./Footer/FooterComponent";
import HeaderComponent from "./Header/HeaderComponent";

function Pizza365() {
    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        {/* <!-- Header */}
                        {/* Navbar menu */}
                        <HeaderComponent></HeaderComponent>
                    </div>
                </div>
            </div>
            {/* Body */}
            <div className="container">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="row">
                            {/* Content */}
                            <ContentComponent></ContentComponent>
                        </div>
                    </div>
                </div>
            </div>
            {/* <!-- Footer --> */}
            <FooterComponent></FooterComponent>
        </div>
    )
}

export default Pizza365