import DrinkComponent from "./DrinkComponent/DrinkComponent";
import FormComponent from "./FormComponent/FormComponent";
import IntroduceComponent from "./IntroduceComponent/IntroduceComponent";
import SizeComponent from "./SizeComponent/SizeComponent";
import TypeComponent from "./TypeComponent/TypeComponent";

function ContentComponent () {
    return (
        <div>
            <IntroduceComponent></IntroduceComponent>
            <SizeComponent></SizeComponent>
            <TypeComponent></TypeComponent>
            <DrinkComponent></DrinkComponent>
            <FormComponent></FormComponent>
        </div>
    )
}

export default ContentComponent;

