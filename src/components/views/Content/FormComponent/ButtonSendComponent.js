import {
    Modal,
    Container,
    Grid,
    TextField,
    Typography,
    Box,
    Button,
    FormLabel,
    Snackbar,
    Alert
} from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchVoucherAPI, fetchOrderAPI } from "../../../actions/BaseUrlAPIAction"

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};


function ButtonSendComponent() {

    const dispatch = useDispatch();
    const { loaiPizza } = useSelector((reduxData) => reduxData.PizzaTypeReducer);
    const { kichCo, duongKinh, suon, salad, soLuongNuoc, thanhTien } = useSelector((reduxData) => reduxData.PizzaComboSelectReducer);
    const { drinkSelected } = useSelector((reduxData) => reduxData.DrinkReducer);
    const { fullName, email, phoneNumber, address, maGiamGia, message } = useSelector((reduxData) => reduxData.CreateOrderFormReducer);
    const { voucherResult } = useSelector((reduxData) => reduxData.VoucherReducer)
    const { orders } = useSelector((reduxData) => reduxData.OrderFormReducer)

    const [openModalOrderForm, setOpenModalOrderForm] = useState(false);
    const [openModalConfirmForm, setOpenModalConfirmForm] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [alertContent, setAlertContent] = useState("");
    const [alertStatus, setAlertStatus] = useState("error")
    const [orderCode, setOrderCode] = useState("");

    const fetchAPIVoucher = async (url) => {
        const response = await fetch(url);
        const data = response.json();
        return data
    }

    const fetchAPIOrderCreate = async (url, body) => {
        const response = await fetch(url, body);
        const data = response.json();
        return data
    }

    const closeModalOrderForm = () => {
        setOpenModalOrderForm(false)
    }

    const closeModalConfirmForm = () => {
        setOpenModalConfirmForm(false)
    }

    const closeModalAlert = () => {
        setOpenAlert(false)
    }


    const onBtnSendOrder = () => {

        var vOrderObject = {
            kichCo: kichCo,
            duongKinh: duongKinh,
            suon: suon,
            salad: salad,
            soLuongNuoc: soLuongNuoc,
            loaiPizza: loaiPizza,
            idVoucher: maGiamGia,
            thanhTien: thanhTien,
            idLoaiNuocUong: drinkSelected,
            hoTen: fullName,
            email: email,
            soDienThoai: phoneNumber,
            diaChi: address,
            loiNhan: message,
            phanTramGiamGia: -1,
            phaiThanhToan: -1
        };

        var vCheckOrder = validate(vOrderObject);

        if (vCheckOrder) {
            console.log(vOrderObject);
            if (vOrderObject.idVoucher !== "") {
                fetchAPIVoucher("http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" + vOrderObject.idVoucher)
                    .then((data) => {
                        vOrderObject.phanTramGiamGia = data.phanTramGiamGia;
                        vOrderObject.phaiThanhToan = vOrderObject.thanhTien * (1 - vOrderObject.phanTramGiamGia / 100);
                        console.log("Voucher input is valid");
                        setAlertStatus("success")
                        console.log(vCheckOrder)
                        setOpenAlert(true)
                        setAlertContent("Data Input is valid");
                        setOpenModalOrderForm(true)
                    })
                    .catch((error) => console.log(error.message))
            } else {
                vOrderObject.phanTramGiamGia = 0;
                vOrderObject.phaiThanhToan = vOrderObject.thanhTien * (1 - vOrderObject.phanTramGiamGia / 100);
                console.log("Voucher id hasn't been entered");
                setAlertStatus("success")
                setOpenAlert(true)
                setAlertContent("Data Input is valid");
                setOpenModalOrderForm(true)
            }
        }
    }

    const validate = (paramOrderObject) => {
        if (paramOrderObject.kichCo === "") {
            setOpenAlert(true);
            setAlertStatus("error");
            setAlertContent("Please select Pizza Combo before submitting!")
            return false
        } else if (paramOrderObject.loaiPizza === "") {
            setOpenAlert(true);
            setAlertStatus("error");
            setAlertContent("Please select Pizza Type before submitting!")
            return false
        } else if (paramOrderObject.drinkSelected === "") {
            setOpenAlert(true);
            setAlertStatus("error");
            setAlertContent("Please select Drink before submitting!")
            return false
        } else if (paramOrderObject.hoTen === "") {
            setOpenAlert(true);
            setAlertStatus("error");
            setAlertContent("Please input Name before submitting!")
            return false
        } else if (paramOrderObject.email === "") {
            setOpenAlert(true);
            setAlertStatus("error");
            setAlertContent("Please input Email before submitting!")
            return false
        } else if (paramOrderObject.soDienThoai === "") {
            setOpenAlert(true);
            setAlertStatus("error");
            setAlertContent("Please input Phone Number before submitting!")
            return false
        } else if (paramOrderObject.diaChi === "") {
            setOpenAlert(true);
            setAlertStatus("error");
            setAlertContent("Please input Address before submitting!")
            return false
        }
        return true;
    }

    const onBtnConfirmOrder = () => {
        setOpenModalOrderForm(false)
        setOpenModalConfirmForm(true)

        const body = {
            method: "POST",
            body: JSON.stringify({
                hoTen: fullName,
                email: email,
                soDienThoai: phoneNumber,
                diaChi: address,
                idVourcher: maGiamGia,
                loiNhan: message,
                thanhTien: thanhTien,
                soLuongNuoc: soLuongNuoc,
                kichCo: kichCo,
                duongKinh: duongKinh,
                suon: suon,
                salad: salad,
                loaiPizza: loaiPizza,
                loaiNuocUong: drinkSelected
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }

        fetchAPIOrderCreate("http://203.171.20.210:8080/devcamp-pizza365/orders", body)
            .then((data) => {
                console.log(data)
                setOrderCode(data.orderCode);
                setOpenAlert(true);
                setAlertContent("Order was updated successfully!!");
                setAlertStatus("success");
            })

    }



    return (
        <>
            <button className="btn w-100" style={{ backgroundColor: "orange", fontWeight: "bold" }} id="btn-gui-don" onClick={onBtnSendOrder}>Gửi</button>

            {/* Alert */}
            <Snackbar
                open={openAlert}
                autoHideDuration={3000}
                onClose={closeModalAlert}
            >
                <Alert onClose={closeModalAlert} severity={alertStatus}>{alertContent}</Alert>
            </Snackbar>

            {/* Modal Thông tin đơn hàng*/}
            <Modal
                open={openModalOrderForm}
                onClose={closeModalOrderForm}
                aria-labelledby="modal-modal-order"
                aria-describedby="modal-modal-create-order"
            >
                <Container fullWidth>
                    <Box sx={style}>
                        <Typography variant="h5" component="h2">
                            <b>Thông Tin Đơn Hàng</b>
                        </Typography>
                        <Grid container mt={2}>
                            <Grid item xs={3} sm={3} md={3} lg={3}>
                                <FormLabel>Họ và tên</FormLabel>
                            </Grid>
                            <Grid item xs={3} sm={3} md={3} lg={3}>
                                <TextField fullWidth variant="outlined" label="Họ và tên" value={fullName} disabled></TextField>
                            </Grid>
                            <Grid item xs={2} sm={2} md={2} lg={2} textAlign="center">
                                <FormLabel>Email</FormLabel>
                            </Grid>
                            <Grid item xs={4} sm={4} md={4} lg={4}>
                                <TextField fullWidth variant="outlined" label="Email" value={email} disabled></TextField>
                            </Grid>
                        </Grid>
                        <Grid container mt={1}>
                            <Grid item xs={3} sm={3} md={3} lg={3}>
                                <FormLabel>Số Điện Thoại</FormLabel>
                            </Grid>
                            <Grid item xs={9} sm={9} md={9} lg={9}>
                                <TextField fullWidth variant="outlined" label="Số Điện Thoại" value={phoneNumber} disabled></TextField>
                            </Grid>
                        </Grid>
                        <Grid container mt={1}>
                            <Grid item xs={3} sm={3} md={3} lg={3}>
                                <FormLabel>Địa Chỉ</FormLabel>
                            </Grid>
                            <Grid item xs={9} sm={9} md={9} lg={9}>
                                <TextField fullWidth variant="outlined" label="Địa Chỉ" value={address} disabled></TextField>
                            </Grid>
                        </Grid>
                        <Grid container mt={1}>
                            <Grid item xs={3} sm={3} md={3} lg={3}>
                                <FormLabel>Mã Giảm Giá</FormLabel>
                            </Grid>
                            <Grid item xs={3} sm={3} md={3} lg={3}>
                                <TextField fullWidth variant="outlined" label="Mã Giảm Giá" value={maGiamGia} disabled></TextField>
                            </Grid>
                            <Grid item xs={2} sm={2} md={2} lg={2} textAlign="center">
                                <FormLabel>Thành Tiền</FormLabel>
                            </Grid>
                            <Grid item xs={4} sm={4} md={4} lg={4}>
                                <TextField fullWidth variant="outlined" label="Thành Tiền" value={thanhTien} disabled></TextField>
                            </Grid>
                        </Grid>
                        <Grid container mt={1}>
                            <Grid item xs={3} sm={3} md={3} lg={3}>
                                <FormLabel>Kích cỡ</FormLabel>
                            </Grid>
                            <Grid item xs={3} sm={3} md={3} lg={3}>
                                <TextField fullWidth variant="outlined" label="Combo" value={kichCo} disabled></TextField>
                            </Grid>
                            <Grid item xs={2} sm={2} md={2} lg={2} textAlign="center">
                                <FormLabel>Đồ Uống</FormLabel>
                            </Grid>
                            <Grid item xs={4} sm={4} md={4} lg={4}>
                                <TextField fullWidth variant="outlined" label="Đồ Uống" value={drinkSelected} disabled></TextField>
                            </Grid>
                        </Grid>
                        <Grid container mt={1}>
                            <Grid item xs={3} sm={3} md={3} lg={3}>
                                <FormLabel>Loại Pizza</FormLabel>
                            </Grid>
                            <Grid item xs={9} sm={9} md={9} lg={9}>
                                <TextField fullWidth variant="outlined" label="Loại Pizza" value={loaiPizza} disabled></TextField>
                            </Grid>
                        </Grid>
                        <Grid container mt={2}>
                            <Grid item xs={3} sm={3} md={3} lg={3}>
                                <FormLabel>Lời Nhắn</FormLabel>
                            </Grid>
                            <Grid item xs={9} sm={9} md={9} lg={9}>
                                <TextField fullWidth variant="outlined" label="Lời Nhắn" value={message} disabled></TextField>
                            </Grid>
                        </Grid>
                        <Grid container mt={1}>
                            <Grid item xs={12} textAlign="end">
                                <Button color="success" variant="contained" className="me-1" onClick={onBtnConfirmOrder}>Xác nhận</Button>
                                <Button color="error" variant="contained" onClick={closeModalOrderForm}>Cancel</Button>
                            </Grid>
                        </Grid>
                    </Box>
                </Container>
            </Modal>

            {/* Modal xác nhận tạo đơn hàng */}
            <Modal
                open={openModalConfirmForm}
                onClose={closeModalConfirmForm}
                aria-labelledby="modal-modal-confirm-order"
                aria-describedby="modal-modal-confirm-order"
            >
                <Box sx={style}>
                    <Typography variant="h6" component="h2">
                        <b>Cảm ơn bạn đã đặt hàng tại Pizza 365. Mã đơn hàng của bạn là:</b>
                    </Typography>
                    <Grid container mt={2}>
                        <Grid item xs={4} sm={4} md={4} lg={4}>
                            <FormLabel>Mã đơn hàng:</FormLabel>
                        </Grid>
                        <Grid item xs={8} sm={8} md={8} lg={8}>
                            <TextField fullWidth variant="outlined" value={orderCode} disabled></TextField>

                        </Grid>
                    </Grid>
                </Box>
            </Modal>
        </>
    )
}

export default ButtonSendComponent