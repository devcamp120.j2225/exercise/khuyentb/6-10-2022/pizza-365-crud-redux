import { useDispatch, useSelector } from "react-redux"
import {nameInput, emailInput, phoneNumberInput, addressInput, voucherInput, messageInput} from "../../../actions/OrderFormAction"
import ButtonSendComponent from "./ButtonSendComponent";

function FormComponent() {
    const dispatch = useDispatch();
    const {fullName, email, phoneNumber, address, maGiamGia, message} = useSelector((reduxData) => reduxData.CreateOrderFormReducer);

    const inputNameChangeHandler = (event) => {
        dispatch(nameInput(event.target.value))
    }

    const inputEmailChangeHandler = (event) => {
        dispatch(emailInput(event.target.value))
    }

    const inputPhoneNumberChangeHandler = (event) => {
        dispatch(phoneNumberInput(event.target.value))
    }

    const inputAddressChangeHandler = (event) => {
        dispatch(addressInput(event.target.value))
    }

    const inputVoucherChangeHandler = (event) => {
        dispatch(voucherInput(event.target.value))
    }

    const inputMessageChangeHandler = (event) => {
        dispatch(messageInput(event.target.value))
    }


    return (
        <>
            {/* <!-- Title Gửi đơn hàng --> */}
            <div className="col-sm-12 text-center p-4 mt-4">
                <h2 style={{color: "orange"}}><b className="p-2 border-bottom border-warning">Gửi đơn hàng</b></h2>
            </div>
            {/* <!-- Content Input form gửi đơn hàng --> */}
            <div className="col-sm-12 mb-5">
                <div className="form-group">
                    <label htmlFor="name" className="pb-2">Tên</label>
                    <input className="form-control" type="text" id="inp-name" placeholder="Nhập tên" onChange={inputNameChangeHandler} value={fullName}/>
                </div>
                <div className="form-group py-4">
                    <label htmlFor="email" className="pb-2">Email</label>
                    <input className="form-control" type="text" id="inp-email" placeholder="Nhập email" onChange={inputEmailChangeHandler} value={email}/>
                </div>
                <div className="form-group">
                    <label htmlFor="phone-number" className="pb-2">Số điện thoại</label>
                    <input className="form-control" type="text" id="inp-phone" placeholder="Nhập số điện thoại" onChange={inputPhoneNumberChangeHandler} value={phoneNumber}/>
                </div>
                <div className="form-group py-4">
                    <label htmlFor="address" className="pb-2">Địa chỉ</label>
                    <input className="form-control" type="text" id="inp-address" placeholder="Nhập địa chỉ" onChange={inputAddressChangeHandler} value={address}/>
                </div>
                <div className="form-group">
                    <label htmlFor="voucher-id" className="pb-2">Mã giảm giá</label>
                    <input className="form-control" type="text" id="inp-voucher-id" placeholder="Nhập mã giảm giá" onChange={inputVoucherChangeHandler} value={maGiamGia}/>
                </div>
                <div className="form-group py-4">
                    <label htmlFor="message" className="pb-2">Lời nhắn</label>
                    <input className="form-control" type="text" id="inp-message" placeholder="Nhập lời nhắn" onChange={inputMessageChangeHandler} value={message}/>
                </div>
                {/* Submit button */}
                
                <ButtonSendComponent></ButtonSendComponent>
            </div>
        </>
    )
}

export default FormComponent