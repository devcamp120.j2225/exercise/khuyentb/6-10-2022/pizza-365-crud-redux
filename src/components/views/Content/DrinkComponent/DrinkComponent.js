import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux"
import { fetchDrinkAPI, DrinkSelect } from "../../../actions/BaseUrlAPIAction"

function DrinkComponent() {

    const dispatch = useDispatch();
    const { drinks, drinkSelected } = useSelector((reduxData) => reduxData.DrinkReducer);

    useEffect(() => {
        dispatch(fetchDrinkAPI())
    }, []);

    console.log("Drinks state:");
    console.log(drinks);
    console.log("Drinks selected value:");
    console.log(drinkSelected);



    const onChangeDrinkHandler = (event) => {
        console.log(event);
        dispatch(DrinkSelect(event.target.value));
    }
    return (
        <>
            {/* <!-- Select a drink --> */}
            <div className="col-sm-12 text-center p-4 mt-4">
                <h2 style={{ color: "orange" }}><b className="border-bottom border-warning">Chọn đồ uống</b></h2>
            </div>
            <select id="select-drink-list" className="form-control" onChange={onChangeDrinkHandler}>
                <option defaultValue> -- Tất cả loại nước uống -- </option>
                {
                    drinks.map((element, index) => {
                        return <option key={index} value={element.maNuocUong}>{element.tenNuocUong}</option>
                    })
                }
            </select>
        </>
    )
}

export default DrinkComponent