import seafoodImage from "../../../../assets/images/seafood.jpg";
import hawaiiImage from "../../../../assets/images/hawaiian.jpg";
import baconImage from "../../../../assets/images/bacon.jpg";
import { useDispatch, useSelector } from "react-redux";
import {selectTypeBacon, selectTypeHawaii, selectTypeSeafood} from "../../../actions/PizzaTypeAction"

function TypeComponent() {

    const dispatch = useDispatch();
    const {loaiPizza} = useSelector((reduxData) => reduxData.PizzaTypeReducer);

    console.log(loaiPizza);

    const onBtnSeafoodClick = () => {
        console.log('Btn Seafood was clicked!');
        dispatch(selectTypeSeafood());
        console.log(selectTypeSeafood())
    }

    const onBtnHawaiiClick = () => {
        console.log('Btn Hawaii was clicked!');
        dispatch(selectTypeHawaii());
        console.log(selectTypeHawaii())
    }

    const onBtnBaconClick = () => {
        console.log('Btn Bacon was clicked!');
        dispatch(selectTypeBacon());
        console.log(selectTypeBacon())
    }
    return (
        <>
            {/* <!-- Title Pizza Type --> */}
            <div id="pizza-type" className="row">
                <div className="col-sm-12 text-center p-4 mt-4">
                    <h2 style={{color: "orange"}}><b className="p-2 border-bottom border-warning">Chọn loại Pizza</b></h2>
                </div>
                {/* <!-- Content Chọn loại Pizza --> */}
                <div className="col-sm-12">
                    <div className="row">
                        <div className="col-sm-4">
                            <div className="card w-100" style={{ width: "18rem" }}>
                                <img src={seafoodImage} alt="seafood" className="card-img-top" />
                                <div className="card-body">
                                    <h3>OCEAN MANIA</h3>
                                    <p>PIZZA HẢI SẢN XỐT MAYONNAISE</p>
                                    <p>Xốt Cà Chua, Phô Mai Mozzaella, Tôm, Mực, Thanh Cua, Hành Tây.
                                    </p>
                                </div>
                                <div className="card-footer text-center">
                                    <button onClick={onBtnSeafoodClick} className="btn w-100" style={{ backgroundColor: "orange", fontWeight: "bold" }} id="btn-chon-seafood">Chọn</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-4">
                            <div className="card w-100" style={{ width: "18rem" }}>
                                <img src={hawaiiImage} alt="hawaii" className="card-img-top" />
                                <div className="card-body">
                                    <h3>HAWAIIAN</h3>
                                    <p>PIZZA DĂM BÔNG DỨA KIỂU HAWAII</p>
                                    <p>Xốt Cà Chua, Phô Mai Mozzaella, Thịt Dăm Bông, Thơm.
                                    </p>
                                </div>
                                <div className="card-footer text-center">
                                    <button onClick={onBtnHawaiiClick} className="btn w-100" style={{ backgroundColor: "orange", fontWeight: "bold" }} id="btn-chon-hawaii">Chọn</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-4">
                            <div className="card w-100" style={{ width: "18rem" }}>
                                <img src={baconImage} alt="bacon" className="card-img-top" />
                                <div className="card-body">
                                    <h3>CHEESY CHICKEN BACON</h3>
                                    <p>PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
                                    <p>Xốt Phô Mai, Thịt Gà, Thịt Heo Muối, Phô Mai Mozzaella, Cà Chua.
                                    </p>
                                </div>
                                <div className="card-footer text-center">
                                    <button onClick={onBtnBaconClick} className="btn w-100" style={{ backgroundColor: "orange", fontWeight: "bold" }} id="btn-chon-bacon">Chọn</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default TypeComponent