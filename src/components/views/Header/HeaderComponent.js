function HeaderComponent() {
    return (
        <>
            <nav className="navbar fixed-top navbar-expand-lg navbar-light mb-2" style={{backgroundColor: "orange"}}>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav nav-fill w-100">
                        <li className="nav-item active">
                            <a className="nav-link active" href="#home" style={{fontWeight: "bold"}}> Trang chủ <span className="sr-only"></span></a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#combo" style={{fontWeight: "bold"}}> Combo </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#pizza-type" style={{fontWeight: "bold"}}> Loại pizza </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#send-order" style={{fontWeight: "bold"}}> Gửi đơn hàng </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </>
    )
}

export default HeaderComponent;